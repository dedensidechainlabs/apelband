<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <meta name="Title" content="Apel Band Official Website">
	<meta name="Author" content="Apel Band">
	<meta name="Subject" content="Apel Band Official Website">
	<meta name="Description" content="Apel Band Official Website">
	<meta name="Keywords" content="Apel Band, Karma Cinta, Apelband, apel band, apel, band">
	<meta name="Language" content="English">
	<meta name="Copyright" content="Copyright 2015, Apel Band, Powered by Sidechain Labs">
	<meta name="Designer" content="Apel Band, Sidechain Labs">
	<meta name="Publisher" content="Apel Band, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.png" />

    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	
	<!-- Add jQuery library -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".fancybox").fancybox();
		});
		$(document).ready(function() {
			$('.fancybox-media').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					media : {}
				}
			});
		});
	</script>
</head>
  <body style="background: url(<?php echo base_url(); ?>assets/images/<?php echo $background; ?>) no-repeat center center fixed;">
    <?php $this->load->view("vheader.php");?>
	
	<!--<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		</ol>

		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<img src="<?php echo base_url();?>assets/images/1bg.jpg" alt="Apel Band">
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>assets/images/2bg.jpg" alt="Apel Band">
			</div>
		</div>

		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>-->
	<br><br><br><br><br><br><br>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="jumbotron">
					<h1><span>Apel Band</span></h1>
					<h2><span>Welcome to Our Official Website</span></h2>
					<h3><span>Karma Cinta</span></h3>
					<audio controls>
						<source src="<?php echo base_url();?>assets/mp3/apel-karma-cinta.mp3" type="audio/mpeg"/>
					</audio>
				</div>
			</div>
		</div>
	</div>
	<br>	
	<br>	
	<br>	
	<div class="container">
		<div class="row">
			<div class="col-lg-9">
				<div class="panel">
					<div class="title">
						<h4>LATEST <span style="font-weight: bold">UPDATES</span></h4>
					</div>
					<section id="blog-landing">
						<?php 
							foreach($content as $row):
							if($row->CONTENTTYPE=="images"){
						?>	
						<article class="white-panel"> 
							<a class="fancybox" rel="group" href="<?php echo base_url().'assets/images/content/'.$row->CONTENTIMAGES;?>" title="<?php echo $row->CONTENTTITLE; ?>">
								<img src="<?php echo base_url().'assets/images/content/'.$row->CONTENTIMAGES;?>" alt="<?php echo $row->CONTENTTITLE; ?>" />
							</a>
						</article>
						<?php
							}else if($row->CONTENTTYPE=="videos"){
						?>
							<article class="white-panel">
								
								<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?php echo $row->CONTENTFULL;?>">
									<img class="video-cover" src="http://img.youtube.com/vi/<?php echo $row->CONTENTFULL; ?>/0.jpg" alt="<?php echo $row->CONTENTTITLE; ?>">
									<img style="width: 20%;" class="video-play-ctrl" src="<?php echo base_url();?>assets/images/overlay-play-btn.png" alt="">
								</a>
							</article>
						<?php
							}
							endforeach;
						?>
					</section>
					<!-- <div class="text-center">
						<a href="<?php echo base_url(); ?>gallery"><button class="btn btn-warning">VIEW MORE</button></a>
						<br><br>
					</div> -->

				</div>

				<div class="panel">
					<div class="title">
						<h4>LATEST <span style="font-weight: bold">STORY</span> &nbsp; &nbsp; <a href="<?php echo base_url(); ?>apelstory"><button class="btn btn-warning">MORE STORY</button></a></h4>
					</div>
					<div class="row" style="padding:0 15px;">
					<?php foreach($latestStory as $rows): ?>
						<div class="col-md-4">
							<div class="panel story">
								<a class="fancybox" rel="groups" href="<?php echo base_url().'assets/images/apelstory/'.$rows->STORYIMAGES;?>" title="<?php echo $rows->STORYTITLE; ?>">
									<img id="img-story" src="<?php echo base_url().'assets/images/apelstory/'.$rows->STORYIMAGES;?>" alt="<?php echo $rows->STORYTITLE; ?>" />
								</a>
								<div class="title">
									<h4><?php echo $rows->STORYTITLE?></h4>
									<h6><?php echo date("D, d M Y",strtotime($rows->STORYDATE));  ?></h6>
								</div>
								<div class="content-paragraf">
									<!--<p><?php echo substr($rows->STORYCONTENT,0,100).". . ."; ?></p>-->
									<br>
									<a href="<?php echo base_url()."apelstory/detail/".$rows->STORYSLUG; ?>/"><button class="btn btn-warning">READ MORE</button></a>
								</div>
								<br>
								<br>
								
							</div>	
						</div>
						
						<?php endforeach; ?>
					</div>
				</div>

			</div>
			
			<div class="col-lg-3">
				<div class="panel">
					<div class="title">
						<h4>SCHEDULE</h4>
					</div>
					<div class="schedule">
						<div class="table-responsive">
							<table id="table-schedule" class="table">
								<?php foreach($qschedule as $row): ?>
								<tr>
									<td class="table-schedule-date"><?php echo date('d M',strtotime($row->SCHEDULEDATE)); ?><br><?php echo date('Y',strtotime($row->SCHEDULEDATE)); ?></td>
									<td style="color:#fc4fd5; background-color: #333;"><?php echo $row->SCHEDULETITLE; ?></td>
								</tr>
								<?php endforeach; ?>
							</table>
						</div>
					</div>
				</div>
				<br>
				<div class="panel">
					<div class="title">
						<h4>SOUNDCLOUD</h4>
					</div>
					<?php 
						foreach($qplaylist as $row): 
						echo $row->PLAYLISTEMBED;
						echo "<br>";
						endforeach;
					?>
				</div>
				<br>
				
				<div class="panel">
					<div class="title">
						<h4>TWITTER</h4>
					</div>
					<a class="twitter-timeline"  href="https://twitter.com/Apel_Band" data-widget-id="548153404850069505">Tweets by @Apel_Band</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
				<br>
				<div class="panel">
					<div class="title">
						<h4>FACEBOOK</h4>
					</div>
					<div class="text-center">
						<div class="fb-like-box" data-href="https://www.facebook.com/1528956900709754" data-width="250" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
						<br>
					</div>
				</div>
				<br>
				<!-- <div class="panel">
					<div class="title">
						<h4>SUBSCRIBE</h4>
					</div>
					<div class="text-left" style="padding: 0 15px;">
						<form>
						  <div class="form-group">
							<input type="email" class="form-control" id="exampleInputPassword1" placeholder="Enter Email">
						  </div>
						  <button type="submit" class="btn btn-warning">Submit</button>
						</form>
						<br>
					</div>
				</div> -->
				<br><br><br>
			</div>
		</div>
	</div>
	<br>
	
	<?php $this->load->view('vfooter.php');?>

	
	<!--<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>-->
	<script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 3,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=490590447735463&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

</html>