<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
     <meta name="Title" content="Apel Band Official Website">
	<meta name="Author" content="Apel Band">
	<meta name="Subject" content="Apel Band Official Website">
	<meta name="Description" content="Apel Band Official Website">
	<meta name="Keywords" content="Apel Band, Karma Cinta, Apelband, apel band, apel, band">
	<meta name="Language" content="English">
	<meta name="Copyright" content="Copyright 2015, Apel Band, Powered by Sidechain Labs">
	<meta name="Designer" content="Apel Band, Sidechain Labs">
	<meta name="Publisher" content="Apel Band, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.png" />
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	
	
  </head>
  <body style="background: url(<?php echo base_url(); ?>assets/images/<?php echo $background; ?>) no-repeat center center fixed;">
    <?php $this->load->view("vheader.php");?>
	<br><br><br>
	<div class="container">
		<div class="panel">
			<div class="row">
				<div class="col-lg-12">
					<div class="title">
						<h4>CONTACT <span style="font-weight: bold">US</span></h4>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="content-paragraf">
						<form>
						  <div class="form-group">
							<label for="exampleInputEmail1">Name</label>
							<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
						  </div>
						  <div class="form-group">
							<label for="exampleInputPassword1">Email</label>
							<input type="email" class="form-control" id="exampleInputPassword1" placeholder="Enter Email">
						  </div>
						  <div class="form-group">
							<label for="exampleInputEmail1">Subject</label>
							<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Subject">
						  </div>
						  <div class="form-group">
							<label for="exampleInputEmail1">Message</label>
							<textarea  class="form-control" >Enter Message</textarea>
						  </div>
						  <button type="submit" class="btn btn-warning">Send</button>
						</form>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="content-paragraf">
						<h2>Contact <span style="font-weight: bold">Info</span></h2><br>
						<h4>Apel Band<br><br>
							Z-Three Music Studio<br><br>
							Jl. Kertamukti Pisangan Barat Raya no.20<br><br>
							Cirendeu - Ciputat<br><br>
							Tangerang Selatan 15419<br><br>
							CP: +62 81 555 747 077 (Cici)<br><br>
							Email: info@apelband.com<br><br>
						</h4>
					</div>
				</div>
			</div>
			<br><br><br>
		</div>
	</div>
	<br><br>
	<?php $this->load->view('vfooter.php');?>

	
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 3,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>