<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
     <meta name="Title" content="Apel Band Official Website">
	<meta name="Author" content="Apel Band">
	<meta name="Subject" content="Apel Band Official Website">
	<meta name="Description" content="Apel Band Official Website">
	<meta name="Keywords" content="Apel Band, Karma Cinta, Apelband, apel band, apel, band">
	<meta name="Language" content="English">
	<meta name="Copyright" content="Copyright 2015, Apel Band, Powered by Sidechain Labs">
	<meta name="Designer" content="Apel Band, Sidechain Labs">
	<meta name="Publisher" content="Apel Band, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.png" />
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	
	
  </head>
  <body style="background: url(<?php echo base_url(); ?>assets/images/<?php echo $background; ?>) no-repeat center center fixed;">
    <?php $this->load->view("vheader.php");?>
	<br><br>
	<div class="container">
		<div class="panels">
			
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					
					<?php foreach($qstory as $row): ?>
					
						<div class="panel" style="background: rgba(0,0,0,0.9);">
							<img style="width:100%;" src="<?php echo base_url(); ?>assets/images/apelstory/<?php echo $row->STORYIMAGES;?>" class="img">
							<div class="title text-center">
								<h3><?php echo $row->STORYTITLE?></h3>
								<h6><?php echo date("D, d M Y",strtotime($row->STORYDATE));  ?></h6><br>
							</div>
							<div class="content-paragraf">
								<p><?php echo $row->STORYCONTENT; ?></p>
								
							</div>
							<div class="text-center">
								<a class="twitter-share-button"
   href="https://twitter.com/share"
  data-url="<?php echo base_url()."apelstory/detail/".$row->STORYSLUG; ?>"
  data-via="apel_band"
  data-text="<?php echo $row->STORYTITLE; ?>"
  data-related="twitterdev:Twitter Developer Relations"
  data-count="vertical">
Tweet
</a>
<script>
window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
</script>
<br>
								<!-- <a href="<?php echo base_url()."apelstory/";?>"><button class="btn btn-warning"> BACK TO APEL STORY LIST</button></a> -->
							</div>
							<br><br>
						</div>	
					
					<?php endforeach; ?>
					
					<div class="panel">
						<div class="title">
							<h4>YOU MAY LIKE ALSO</h4>
						</div>
						<div class="row" style="padding:0 15px;">
						<?php foreach($randomStory as $rows): ?>
							<div class="col-md-4">
								<div class="panel story">
									<a class="fancybox" rel="groups" href="<?php echo base_url().'assets/images/apelstory/'.$rows->STORYIMAGES;?>" title="<?php echo $rows->STORYTITLE; ?>">
										<img id="img-story" src="<?php echo base_url().'assets/images/apelstory/'.$rows->STORYIMAGES;?>" alt="<?php echo $rows->STORYTITLE; ?>" />
									</a>
									<div class="title">
										<h4><?php echo $rows->STORYTITLE?></h4>
										<h6><?php echo date("D, d M Y",strtotime($rows->STORYDATE));  ?></h6>
									</div>
									<div class="content-paragraf">
										<!--<p><?php echo substr($rows->STORYCONTENT,0,100).". . ."; ?></p>-->
										<br>
										<a href="<?php echo base_url()."apelstory/detail/".$rows->STORYSLUG; ?>/"><button class="btn btn-warning">READ MORE</button></a>
									</div>
									<br>
									<br>
									
								</div>	
							</div>
							
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
			<br><br>
		</div>
	</div>
	<br><br>
	<?php $this->load->view('vfooter.php');?>

	
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 3,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>