<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
     <meta name="Title" content="Apel Band Official Website">
	<meta name="Author" content="Apel Band">
	<meta name="Subject" content="Apel Band Official Website">
	<meta name="Description" content="Apel Band Official Website">
	<meta name="Keywords" content="Apel Band, Karma Cinta, Apelband, apel band, apel, band">
	<meta name="Language" content="English">
	<meta name="Copyright" content="Copyright 2015, Apel Band, Powered by Sidechain Labs">
	<meta name="Designer" content="Apel Band, Sidechain Labs">
	<meta name="Publisher" content="Apel Band, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.png" />
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	
	
  </head>
  <body style="background: url(<?php echo base_url(); ?>assets/images/<?php echo $background; ?>) no-repeat center center fixed;">
    <?php $this->load->view("vheader.php");?>
	<br><br><br>
	<div class="container">
		<div class="panel">
			<div class="row">
				<div class="col-lg-12">
					<div class="title">
						<h4>ABOUT <span style="font-weight: bold">APEL</span></h4>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="content-paragraf-img">
						<img src="<?php echo base_url();?>assets/images/apel-band-profile.jpg" width="100%">
					</div>
				</div>
				<div class="col-lg-6">
					<div class="content-paragraf">
						<br>
						<h4>OUR <span style="font-weight: bold">STORY</span></h4>
						<br><p>Apel band terbentuk atas "keisengan" Tyty (gitar) dan Okta (bass) pada 17 Agustus 2004 di Malang. Band yang terbentuk ketika mereka duduk di bangku sekolah ini semula bernama Paprica dengan mengusung musik Top 40. Paprica yang memulai debut dengan manggung di acara HUT RI ini kemudian semakin serius menggeluti dunia musik karena keunikan konsep yang mereka miliki. "Band dengan semua personil wanita-nya" ini membuat banyak Event Organizer tertarik untuk mengundang mereka ke berbagai acara di Kota Malang dan bahkan di luar Kota Malang.</p>
						<br><p>Selama beberapa tahun, banyak perubahan yang terjadi dalam intern band ini. Pada 2006, Rya (drum & back vocal) bergabung sebagai drummer dan kemudian pada 2007 disusul oleh Ofi (gitar & back vocal). Dengan semakin solidnya formasi band, "keisengan" hobi bermain musik yang mereka geluti ini mulai membuahkan hasil. Kerja keras mereka dari panggung ke panggung ketika itu akhirnya mulai dihargai, bahkan tak jarang Paprica sudah bisa mendapatkan honor yang terbilang lumayan untuk ukuran Kota Malang.</p>
					</div>
				</div>
				
				<div class="col-lg-12">
					<div class="content-paragraf">
						<br>
						<p>Pada 2007, Paprica manggung di sebuah kota di Jawa Timur dan bertemu dengan Dody dan Tama "Kangen band". Setelah perbincangan singkat dengan mereka, personel Paprica sangat termotivasi dan mulai membuat lagu karya sendiri serta mengirim demo lagu mereka ke label musik yang ada di Jakarta. Setelah sekitar 1 tahun menunggu, akhirnya pada akhir 2008 Paprica berkesempatan bertemu dengan Bapak Rahayu Kertawiguna sebagai bos Nagaswara di Malang dan diminta langsung memainkan musik di depan beliau. Ketertarikan Bapak Rahayu akhirnya membawa Paprica hijrah ke ibukota pada Oktober 2008.</p>
						<p>Perubahan konsep dari band yang mengusung lagu-lagu Top 40 menjadi band yang mengusung lagu-lagu sendiri membuat formasi band ini mengalami perubahan. Untuk memenuhi karakter vocal yang sesuai dengan konsep band, maka dilakukanlah audisi vocalist dan akhirnya  terpilihlah Asther (vocal).</p>
						<p>Paprica resmi bergabung dengan Nagaswara pada 2008 dan berganti nama menjadi Apel "G". Nama "Apel" dipilih karena band ini terbentuk di kota Malang, kota yang terkenal dengan buah apel-nya. Sementara huruf "G" yang berarti "girls", menunjukan identitas dari para personelnya yang kesemuanya adalah wanita. Selain itu, nama "Apel" dipilih karena buah apel adalah buah yang tumbuh di dataran tinggi serta buah yang dapat dinikmati semua kalangan masyarakat. Dengan filosofi tersebut band ini berharap semoga mereka dapat tumbuh tinggi sejajar dengan musisi senior yang lebih dahulu menorehkan kesuksesan dan semoga musik Apel "G" dapat diterima oleh semua kalangan masyarakat.</p>
						<p>Setelah resmi bernaung di bawah bendera Nagaswara, Apel "G" sepakat untuk mengusung genre "Pop Candy" sebagai identitas band mereka. Istilah "Pop Candy" mereka pilih karena mereka membawakan musik pop dengan bermacam-macam rasa seperti layaknya sebuah permen yang disukai kebanyakan orang.</p>
						<p>"Girl Power" adalah judul album pertama yang berhasil di-release oleh Apel "G" dan berisi 10 lagu. Sembilan lagu di album pertama diciptakan oleh Apel "G" dan satu lagu yang menjadi single pertama "KPK (Kau Putuskanku)" diciptakan oleh Kenny "Tarzan Boys". Proses rekaman album pertama banyak dibantu oleh Aria Baron sebagai Music Director dan proses rekaman dilakukan di Colloseum Studio, Jakarta.</p>
						<p>Formasi Apel "G" semakin mantap dan lengkap karena pada 2009 Mizthree (Keyboard) bergabung dan memberikan nuansa baru di musik Apel "G". Pada tahun 2010, Apel "G" merilis single ke-2 yaitu Resahku (Cipt. Ofi) yang diambil dari album pertama yang di aransemen ulang dan di-direct oleh Hendry Martin (Koes Plus Junior).</p>
						<p>Pada tahun 2012, band ini sepakat merubah nama Apel "G" menjadi Apel. Hal ini dilakukan untuk mempermudah pelafalan nama band ketika manggung karena pada tahun-tahun sebelumnya banyak sekali orang yang bingung atau bahkan kesulitan melafalkan nama Apel "G" (baca: apel ji). Perubahan nama tersebut ditandai dengan keluarnya single ketiga "Sedang Apa kamu (SAK)" yang diciptakan oleh Capoenx "Asbak" dan perubahan dalam logo Apel.</p>
						<p>Dengan formasi yang bertahan saat ini, Tyty, Okta, Rya, Ofi, Asther dan Mizthree yang tergabung dalam Apel Band kembali me-release single keempat yaitu "Karma Cinta". Single bertempo middle yang berkisah tentang seseorang yang mendapatkan balasannya jika bermain-main dengan sebuah rasa cinta ini diciptakan oleh Yogi "Gaijin". Single bernuansa pop dan sentuhan rock ini dipilih langsung oleh Bapak Rahayu selaku bos Nagaswara dan saat ini sedang menjalani tahap promo di tv dan radio-radio di Indonesia.</p>
						<br><br>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<?php $this->load->view('vfooter.php');?>

	
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 3,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>