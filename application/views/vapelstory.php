<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
     <meta name="Title" content="Apel Band Official Website">
	<meta name="Author" content="Apel Band">
	<meta name="Subject" content="Apel Band Official Website">
	<meta name="Description" content="Apel Band Official Website">
	<meta name="Keywords" content="Apel Band, Karma Cinta, Apelband, apel band, apel, band">
	<meta name="Language" content="English">
	<meta name="Copyright" content="Copyright 2015, Apel Band, Powered by Sidechain Labs">
	<meta name="Designer" content="Apel Band, Sidechain Labs">
	<meta name="Publisher" content="Apel Band, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.png" />
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	
	
  </head>
  <body style="background: url(<?php echo base_url(); ?>assets/images/<?php echo $background; ?>) no-repeat center center fixed;">
  	
    <?php $this->load->view("vheader.php");?>
	<br><br><br>
	<div class="container">
		<div class="panel">
			<div class="row">
				<div class="col-lg-12">
					<div class="title">
						<h4>APEL <span style="font-weight: bold">STORY </span></h4><br>
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					
					<?php foreach($qstory as $row): ?>
					<div class="col-lg-4">
						<div class="panel story">
							<img class="img-story" src="<?php echo base_url(); ?>assets/images/apelstory/<?php echo $row->STORYIMAGES;?>">
							<div class="title">
								<h4><?php echo $row->STORYTITLE?></h4>
								<h6><?php echo date("D, d M Y",strtotime($row->STORYDATE));  ?></h6>
							</div>
							<div class="content-paragraf">
								<p><?php echo substr($row->STORYCONTENT,0,255).". . ."; ?></p>
								<br>
								<a href="<?php echo base_url()."apelstory/detail/".$row->STORYSLUG; ?>/"><button class="btn btn-warning">MORE</button></a>
							</div>
							<br>
							<br>
							
						</div>	
					</div>
					<?php endforeach; ?>
					
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12 text-center">

					<div class="pagination-wrapper">
						<ul class="pagination">
							<li><?php echo $links; ?></li>
						</ul>
					</div>			
				</div>
			</div>
			<br><br>
		</div>
	</div>
	<br><br>
	<?php $this->load->view('vfooter.php');?>

	
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 3,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>