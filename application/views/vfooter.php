<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="copyright">
					<p>Copyright &copy; 2014. Apel Band Official Website. Powered by <a href="http://sidechainlabs.com/" target="_blank">Sidechain Labs.</a></p>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="social-media">
					<a href="https://www.facebook.com/pages/Apel-Band/1528956900709754" target="_blank"><img src="<?php echo base_url();?>assets/images/icon-facebook.png"></a>
					<a href="https://twitter.com/apel_band/" target="_blank"><img src="<?php echo base_url();?>assets/images/icon-twitter.png"></a>
					<a href="http://www.reverbnation.com/apel_id/" target="_blank"><img src="<?php echo base_url();?>assets/images/icon-reverbnation.png"></a>
					<a href="https://www.youtube.com/user/ApelBandIndonesia/videos/" target="_blank"><img src="<?php echo base_url();?>assets/images/icon-youtube.png"></a>
					<a href="https://soundcloud.com/apel_id/" target="_blank"><img src="<?php echo base_url();?>assets/images/icon-soundcloud.png"></a>
					<a href="https://instagram.com/apel_id/" target="_blank"><img src="<?php echo base_url();?>assets/images/icon-instagram.png"></a>
				</div>
			</div>
		</div>
	</div>
</footer>
<script>
  /*
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58130108-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
	*/
</script>
