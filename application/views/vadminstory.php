<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $title;?></title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
</head>
<body>
	<div id="wrapper">
        <?php $this->load->view('vadminmenu');?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Story</h1>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo base_url();?>admin/addStory/"><button class="btn btn-primary">ADD STORY</button></a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Images</th>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Upload Date</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 
											$counter=1;
											foreach($qstory as $row):
										?>
										<tr>
											<td><?php echo $counter; ?></td>
											<td><img src="<?php echo base_url()."assets/images/apelstory/".$row->STORYIMAGES; ?>" width="80%"></td>
											<td><?php echo $row->STORYTITLE; ?></td>
											<td><?php echo substr($row->STORYCONTENT,0,255); ?></td>
											<td><?php echo date("d M Y",strtotime($row->STORYDATE));  ?></td>
											<td>
												<form action="<?php echo base_url();?>admin/editStory/" method="post">
													<input type="hidden" name="storyID" value="<?php echo $row->STORYID; ?>">
													<input type="hidden" name="storyImages" value="<?php echo $row->STORYIMAGES; ?>">
													<input class="btn btn-warning" type="submit" name="editStory" value="EDIT">
												</form>
												<form action="<?php echo base_url();?>admin/deleteStory/" method="post">
													<input type="hidden" name="storyID" value="<?php echo $row->STORYID; ?>">
													<input type="hidden" name="storyImages" value="<?php echo $row->STORYIMAGES; ?>">
													<input class="btn btn-danger" type="submit" name="deleteStory" value="DELETE">
												</form>
											</td>
										</tr>
										<?php 
											$counter++;
											endforeach;
										?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>
	<script>
		$(document).ready(function() {
			$('#dataTables-example').dataTable();
		});
    </script>
</body>
</html>