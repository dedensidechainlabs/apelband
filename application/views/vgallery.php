<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
     <meta name="Title" content="Apel Band Official Website">
	<meta name="Author" content="Apel Band">
	<meta name="Subject" content="Apel Band Official Website">
	<meta name="Description" content="Apel Band Official Website">
	<meta name="Keywords" content="Apel Band, Karma Cinta, Apelband, apel band, apel, band">
	<meta name="Language" content="English">
	<meta name="Copyright" content="Copyright 2015, Apel Band, Powered by Sidechain Labs">
	<meta name="Designer" content="Apel Band, Sidechain Labs">
	<meta name="Publisher" content="Apel Band, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.png" />
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	
	<!-- Add jQuery library -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo base_url();?>assets/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".fancybox").fancybox();
		});
		$(document).ready(function() {
			$('.fancybox-media').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					media : {}
				}
			});
		});
	</script>
  </head>
  <body style="background: url(<?php echo base_url(); ?>assets/images/<?php echo $background; ?>) no-repeat center center fixed;">
    <?php $this->load->view("vheader.php");?>
	<br><br><br>
	<div class="container">
		<div class="panel">
			<div class="row">
				<div class="col-lg-12">
					<div class="title">
						<h4><span style="font-weight: bold">GALLERY</span></h4>
					</div>
					<section id="blog-landing">
						<?php 
							foreach($content as $row):
							if($row->CONTENTTYPE=="images"){
						?>	
						<article class="white-panel"> 
							<a class="fancybox" rel="group" href="<?php echo base_url().'assets/images/content/'.$row->CONTENTIMAGES;?>" title="<?php echo $row->CONTENTTITLE; ?>">
								<img src="<?php echo base_url().'assets/images/content/'.$row->CONTENTIMAGES;?>" alt="<?php echo $row->CONTENTTITLE; ?>" />
							</a>
						</article>
						<?php
							}else if($row->CONTENTTYPE=="videos"){
						?>
							<article class="white-panel">
								
								<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?php echo $row->CONTENTFULL;?>">
									<img class="video-cover" src="http://img.youtube.com/vi/<?php echo $row->CONTENTFULL; ?>/0.jpg" alt="<?php echo $row->CONTENTTITLE; ?>">
									<img style="width: 20%;" class="video-play-ctrl" src="<?php echo base_url();?>assets/images/overlay-play-btn.png" alt="">
								</a>
							</article>
						<?php
							}
							endforeach;
						?>
					</section>
				</div>

			</div>
			<div class="row">
				<div class="col-lg-12 text-center">

					<div class="pagination-wrapper">
						<ul class="pagination">
							<li><?php echo $links; ?></li>
						</ul>
					</div>			
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<?php $this->load->view('vfooter.php');?>

	
	<script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 4,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>