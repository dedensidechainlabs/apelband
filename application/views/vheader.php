<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/images/logo-apel-band.png" id="logo"></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav navbar-right">
				<?php if($this->uri->segment(1)==""){ ?>
					<li class="active"><a href="<?php echo base_url();?>">HOME</a></li>
				<?php }else{ ?>
					<li><a href="<?php echo base_url();?>">HOME</a></li>
				<?php } ?>
				
				<?php if($this->uri->segment(1)=="theband"){ ?>
					<li class="active"><a href="<?php echo base_url();?>theband/">THE BAND</a></li>
				<?php }else{ ?>
					<li><a href="<?php echo base_url();?>theband/">THE BAND</a></li>
				<?php } ?>
				
				<?php if($this->uri->segment(1)=="playlist"){ ?>
					<li class="active"><a href="<?php echo base_url();?>playlist/">PLAYLIST</a></li>
				<?php }else{ ?>
					<li><a href="<?php echo base_url();?>playlist/">PLAYLIST</a></li>
				<?php } ?>
				
				<?php if($this->uri->segment(1)=="gallery"){ ?>
					<li class="active"><a href="<?php echo base_url();?>gallery/">GALLERY</a></li>
				<?php }else{ ?>
					<li><a href="<?php echo base_url();?>gallery/">GALLERY</a></li>
				<?php } ?>
				
				<?php if($this->uri->segment(1)=="apelstory"){ ?>
					<li class="active"><a href="<?php echo base_url();?>apelstory/">APEL STORY</a></li>
				<?php }else{ ?>
					<li><a href="<?php echo base_url();?>apelstory/">APEL STORY</a></li>
				<?php } ?>
				
				<?php if($this->uri->segment(1)=="store"){ ?>
					<li class="active"><a href="<?php echo base_url();?>store/">STORE</a></li>
				<?php }else{ ?>
					<li><a href="<?php echo base_url();?>store/">STORE</a></li>
				<?php } ?>
				
				<?php if($this->uri->segment(1)=="about"){ ?>
					<li class="active"><a href="<?php echo base_url();?>about/">ABOUT</a></li>
				<?php }else{ ?>
					<li><a href="<?php echo base_url();?>about/">ABOUT</a></li>
				<?php } ?>
				
				<?php if($this->uri->segment(1)=="contact"){ ?>
					<li class="active"><a href="<?php echo base_url();?>contact/">CONTACT</a></li>
				<?php }else{ ?>
					<li><a href="<?php echo base_url();?>contact/">CONTACT</a></li>
				<?php } ?>
			  </ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>