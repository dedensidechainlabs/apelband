		<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url();?>" target="_blank"><img src="<?php echo base_url();?>assets/images/logo-apel-band.png" width="45px">&nbsp; Apel Band |  Administrator Page</a>
            </div>
            <!-- /.navbar-header -->

           
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li><a href="<?php echo base_url();?>admin/dashboard/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>                     
						<li><a href="<?php echo base_url();?>admin/background/"><i class="fa fa-dashboard fa-fw"></i> Background</a></li>						
						<li><a href="<?php echo base_url();?>admin/playlist/"><i class="fa fa-dashboard fa-fw"></i> Playlist</a></li>						
						<li><a href="<?php echo base_url();?>admin/gallery/"><i class="fa fa-dashboard fa-fw"></i> Gallery</a></li>					
						<li><a href="<?php echo base_url();?>admin/video/"><i class="fa fa-dashboard fa-fw"></i> Video</a></li>
						<li><a href="<?php echo base_url();?>admin/schedule/"><i class="fa fa-dashboard fa-fw"></i> Schedule</a></li>
						<li><a href="<?php echo base_url();?>admin/story/"><i class="fa fa-dashboard fa-fw"></i> Story</a></li>
						<li><a href="<?php echo base_url();?>admin/logout/"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
					</ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>