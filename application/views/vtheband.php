<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
     <meta name="Title" content="Apel Band Official Website">
	<meta name="Author" content="Apel Band">
	<meta name="Subject" content="Apel Band Official Website">
	<meta name="Description" content="Apel Band Official Website">
	<meta name="Keywords" content="Apel Band, Karma Cinta, Apelband, apel band, apel, band">
	<meta name="Language" content="English">
	<meta name="Copyright" content="Copyright 2015, Apel Band, Powered by Sidechain Labs">
	<meta name="Designer" content="Apel Band, Sidechain Labs">
	<meta name="Publisher" content="Apel Band, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.png" />
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/set2.css" />
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	
	
  </head>
  <body style="background: url(<?php echo base_url(); ?>assets/images/<?php echo $background; ?>) no-repeat center center fixed;">
    <?php $this->load->view("vheader.php");?>
	<br><br><br>
	<div class="container">
		<div class="panel">
			<div class="row">
				<div class="col-lg-12">
					<div class="title">
						<h4>THE <span style="font-weight: bold">BAND</span></h4>
					</div>
				</div>
			</div>
			<div class="row">
				<div style="padding: 0 30px;">
					<div id="asthersticker" class="col-lg-4">
						<div class="content-theband-img">
							<div class="grid">
								<figure class="effect-ming">
									<img src="<?php echo base_url();?>assets/images/theband/sticker-asther-apel.jpg" width="100%">
									<figcaption>
										<p class="text-center"><br><br>Asther<br>Vocal<br><br><button id="astherdetailbutton" class="btn btn-warning">View more</button></p>
									</figcaption>			
								</figure>
							</div>
						</div>
					</div>
					<div id="astherdetail" style="display:none;">
						<div class="col-lg-8">
							<div class="thebanddetail">
								<button id="astherdetailclose" class="btn btn-warning" style="float:right;">Close</button>
								<h3>Asther</h3>
								<p>Astherina Yuni Susilo atau biasa disapa dengan Asther merupakan vocalist Apel kelahiran 4 Juni di Kota Malang. Perempuan ini memulai karir bernyanyi pada saat duduk dibangku sekolah menengah pertama.  Ajang pentas seni, festival band, paduan suara, hingga kejuaraan bernyanyi sudah pernah dijajal oleh Asther. Kegigihan Asther di dunia tarik suara akhirnya membuahkan hasil, ia sempat menjuarai ajang bintang radio RRI dan finalis ajang pencarian bakat di salah satu TV swasta. Sebelum bergabung dengan Apel, perempuan yang memiliki hobi berenang dan pilates ini sudah disibukkan dengan job manggung sebagai penyanyi wedding dan cafe yang lumayan padat. Pertemuan Asther dengan Apel dimulai pada saat audisi pencarian vocalist mengisi kekosongan vocalist di Apel. Semenjak hijrah ke Jakarta, penyanyi yang banyak ter-influence oleh Jessie J. dan Hayley Williams ini mencoba merambah dunia acting dan periklanan diantaranya dia pernah bernyanyi untuk beberapa jingle iklan dan juga sebagai model iklan di TV.</p>
								<p>Facebook : Astherina Junie Susilo Apel</p>
								<p>Twitter : @Astherina_Apel</p>
							</div>
						</div>
						<div class="col-lg-12">
							<img src="<?php echo base_url();?>assets/images/theband/asther-profile.jpg" width="100%">
						</div>
					</div>
					<div id="tytysticker" class="col-lg-4">
						<div class="content-theband-img">
							<div class="grid">
								<figure class="effect-ming">
									<img src="<?php echo base_url();?>assets/images/theband/sticker-tyty-apel.jpg" width="100%">
									<figcaption>
										<p class="text-center"><br><br>Tyty<br>Guitar<br><br><button id="tytydetailbutton" class="btn btn-warning">View more</button></p>
									</figcaption>			
								</figure>
							</div>
						</div>
					</div>
					<div id="tytydetail" style="display:none;">
						<div class="col-lg-8">
							<div class="thebanddetail">
								<button id="tytydetailclose" class="btn btn-warning" style="float:right;">Close</button>
								<h3>Tyty</h3>
								<p>Perempuan kelahiran 22 November yang akrab dipanggil Tyty ini mempunyai andil besar bersama Okta dalam membentuk band Apel. Semenjak di bangku sekolah, perempuan yang mengidolakan Dewa Budjana "GIGI" ini mempunyai visi dan misi dalam membentuk band yang hanya beranggotakan perempuan. Tyty memperdalam ilmunya bermain gitar dari seorang gitaris handal di Kota Malang yaitu Cici yang sekaligus menjadi manager Apel band. Menonton video cover band favoritnya di Youtube adalah salah satu hobi dari perempuan yang lahir di kota Apel ini. Aliran musik britpop menginspirasi Tyty dalam menuangkan alunan nada-nada gitarnya ke dalam lagu Apel. Lagu-lagu milik Oasis, Radiohead dan Coldplay tidak pernah absen dari playlist Mp3-nya. Perjalanan bersama Apel membuat semakin membuat  permainan gitarnya semakin matang hingga saat ini, namun tak membuat Tyty cepat puas dan terus belajar gitar hingga sekarang.</p>
								<p>Facebook : Tiyas Lutfitasari</p>
								<p>Twitter : @tytyapel</p>
								<p>Instagram : @tiyaslutfitasari</p>
							</div>
						</div>
						<div class="col-lg-12">
							<img src="<?php echo base_url();?>assets/images/theband/tyty-profile.jpg" width="100%">
						</div>
					</div>
					<div id="ofisticker" class="col-lg-4">
						<div class="content-theband-img">
							<div class="grid">
								<figure class="effect-ming">
									<img src="<?php echo base_url();?>assets/images/theband/sticker-ofi-apel.jpg" width="100%">
									<figcaption>
										<p class="text-center"><br><br>Ofi<br>Guitar<br><br><button id="ofidetailbutton" class="btn btn-warning">View more</button></p>
									</figcaption>			
								</figure>
							</div>
						</div>
					</div>
					<div id="ofidetail" style="display:none;">
						<div class="col-lg-8">
							<div class="thebanddetail">
								<button id="ofidetailclose" class="btn btn-warning" style="float:right;">Close</button>
								<h3>Ofi</h3>
								<p>Ofi adalah guitarist Apel yang dilahirkan di Tarakan, Kalimantan Utara pada 29 Januari. Perempuan yang memiliki nama lengkap Ofi Nur Iman ini sejak kecil tertarik dengan bernyanyi dan hal ini terbukti dengan beberapa prestasinya menjuarai beberapa festival band di kampung halamannya. Setelah lulus SMA, wanita yang memiliki hobi bermain gitar, menyanyi, dan menulis lagu ini memutuskan hijrah ke Malang. Awal pertemuan dengan Rya (drummer), menjadi awal baru karir bermusiknya di Malang. Menjadi vocalist band Paprica (sekarang Apel) yang cukup terkenal di Jawa Timur membuat pengagum Joe Satriani ini semakin serius dalam bermusik. Selang tiga bulan bergabung, dia tertantang untuk mengisi kekosongan posisi guitarist di band ini dan mulailah dia belajar bermain gitar secara otodidak dengan proses yang begitu cepat. Penyuka musik R&B dan pop alternative ini adalah pencipta lagu �Resahku� yang menjadi single kedua Apel.
								</p>
								<p>Facebook: Ofi Nur Iman</p>
								<p>Twitter: @ofiapelg</p>
								<p>Instagram: @ofiapelband</p>
							</div>
						</div>
						<div class="col-lg-12">
							<img src="<?php echo base_url();?>assets/images/theband/ofi-profile.jpg" width="100%">
						</div>
					</div>
					<div id="oktasticker" class="col-lg-4">
						<div class="content-theband-img">
							<div class="grid">
								<figure class="effect-ming">
									<img src="<?php echo base_url();?>assets/images/theband/sticker-okta-apel.jpg" width="100%">
									<figcaption>
										<p class="text-center"><br><br>Okta<br>Bass<br><br><button id="oktadetailbutton" class="btn btn-warning">View more</button></p>
									</figcaption>			
								</figure>
							</div>
						</div>
					</div>
					<div id="oktadetail" style="display:none;">
						<div class="col-lg-8">
							<div class="thebanddetail">
								<button id="oktadetailclose" class="btn btn-warning" style="float:right;">Close</button>
								<h3>Okta</h3>
								<p>Okta wulan merupakan personel Apel yang ikut merintis dari awal terbentuknya band yang beranggotakan enam orang perempuan ini. Pembetot bass Apel ini lahir pada tanggal 25 Oktober di Kota Malang. Bersama dengan Ty yang awalnya  bermain musik saat di bangku sekolah, perempuan yang hobby menonton film ini kemudian mengembangkan skill dalam bermusiknya dengan belajar otodidak dan berguru kepada teman-teman senior di Kota Malang. Selain itu, Okta juga sering mengikuti bass klinik dari pemain bass terkenal tanah air yang datang ke kota Malang yaitu Bondan, Yoyo "Bassman", Indro "Bass Hero". Permainan bass Jeremmy dari Paramore termasuk yang menjadi inspirasi Okta dalam bermain bass. Musik yang disukainya lebih mengarah ke genre pop-alternatif, dance dan R&B.
								</p> 
								<p>Facebook: Oktawulan888@gmail.com</p>
								<p>Twitter: @oktawulan888</p>
								<p>Instagram: @Officialow</p>
							</div>
						</div>
						<div class="col-lg-12">
							<img src="<?php echo base_url();?>assets/images/theband/okta-profile.jpg" width="100%">
						</div>
					</div>
					<div id="mizthreesticker" class="col-lg-4">
						<div class="content-theband-img">
							<div class="grid">
								<figure class="effect-ming">
									<img src="<?php echo base_url();?>assets/images/theband/sticker-mizthree-apel.jpg" width="100%">
									<figcaption>
										<p class="text-center"><br><br>Mizthree<br>Keyboard<br><br><button id="mizthreedetailbutton" class="btn btn-warning">View more</button></p>
									</figcaption>			
								</figure>
							</div>
						</div>
					</div>
					<div id="mizthreedetail" style="display:none;">
						<div class="col-lg-8">
							<div class="thebanddetail">
								<button id="mizthreedetailclose" class="btn btn-warning" style="float:right;">Close</button>
								<h3>Mizthree</h3>
								<p>Personel terakhir yang bergabung bersama Apel band pada tahun 2009 yang akrab disapa Mizthree ini lahir pada tanggal 9 April di Jakarta. Bergabungnya perempuan penyuka musik gothic metal ini berawal dari seringnya jamming session dan sharing mengenai musik dengan Apel. Perempuan penyuka kucing ini membangun sebuah studio musik di bilangan Ciputat sebagai wadah untuk menyalurkan kreativitas yang kini dijadikan basecamp Apel. Nightwish dan Evanescene merupakan band yang menginspirasi Mizthree. Di tengah kesibukan jadwal menjalani studi di Yayasan Music Jakarta mengambil jurusan piano, Mizthree juga mencoba merambah dunia digital synthesizer untuk diaplikasikan ke dalam karya-karya Apel.
								</p>
								<p>Facebook: Mizthree Flagofhate</p>
								<p>Twitter: @mizthreekeys</p>
								<p>Instagram: @mizthreekeys</p>
							</div>
						</div>
						<div class="col-lg-12">
							<img src="<?php echo base_url();?>assets/images/theband/mizthree-profile.jpg" width="100%">
						</div>
					</div>
					<div id="obenxsticker" class="col-lg-4">
						<div class="content-theband-img">
							<div class="grid">
								<figure class="effect-ming">
									<img src="<?php echo base_url();?>assets/images/theband/sticker-obenx-apel.jpg" width="100%">
									<figcaption>
										<p class="text-center"><br><br>Rya<br>Drum<br><br><button id="obenxdetailbutton" class="btn btn-warning">View more</button></p>
									</figcaption>			
								</figure>
							</div>
						</div>
					</div>
					<div id="obenxdetail" style="display:none;">
						<div class="col-lg-8">
							<div class="thebanddetail">
								<button id="obenxdetailclose" class="btn btn-warning" style="float:right;">Close</button>
								<h3>Rya</h3>
								<p>Ria Antika atau biasa dipanggil Rya adalah penggebuk drum di Apel yang memiliki hobi menulis lagu dan hiking. Ketika masih kecil, wanita kelahiran 23 Mei di Malang ini seringkali mengikuti acara kesenian di sekolah dan kampungnya juga pernah tergabung dalam kelompok karawitan anak-anak. Ketertarikannya dengan drum dimulai ketika dia duduk di bangku SMP dan dia membuktikan keseriusannya dengan pernah menjuarai beberapa festival band di masa sekolah. Karena terbatasnya fasilitas, wanita yang sedang menempuh studi magister  di salah satu PTS Jakarta ini mendalami drum secara otodidak dan berguru ke teman-teman sesama drummer. Setelah hijrah ke Jakarta, drummer yang mengidolakan Cobus Potgieter dan Tomoya "One Ok Rock" ini mendalami drum di Gilang Ramadhan Studio Band.
								</p>
								<p>Facebook : Rya Apel</p>
								<p>Twitter	: @RyaApel</p>
								<p>Instagram : @Ryaapel</p>
							</div>
						</div>
						<div class="col-lg-12">
							<img src="<?php echo base_url();?>assets/images/theband/obenx-profile.jpg" width="100%">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div style="padding: 0 30px;">
					
				</div>
			</div>
			<br><br><br>
		</div>
	</div>
	<br><br>
	<?php $this->load->view('vfooter.php');?>

	
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 3,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<script>
		$("#astherdetailbutton").click(function(){
			$("#tytysticker").hide(1000);
			$("#ofisticker").hide(1000);
			$("#oktasticker").hide(1000);
			$("#mizthreesticker").hide(1000);
			$("#obenxsticker").hide(1000);
			$('#astherdetail').removeClass();
			$("#astherdetail").css("display","block");
			$("#astherdetail").addClass("fadeIn");
		});
		$("#astherdetailclose").click(function(){
			$("#astherdetail").hide(1000);
			$('#astherdetail').removeClass();
			$("#tytysticker").show(1000);
			$("#ofisticker").show(1000);
			$("#oktasticker").show(1000);
			$("#mizthreesticker").show(1000);
			$("#obenxsticker").show(1000);
		}); 
		$("#tytydetailbutton").click(function(){
			$("#asthersticker").hide(1000);
			$("#ofisticker").hide(1000);
			$("#oktasticker").hide(1000);
			$("#mizthreesticker").hide(1000);
			$("#obenxsticker").hide(1000);
			$('#tytydetail').removeClass();
			$("#tytydetail").css("display","block");
			$("#tytydetail").addClass("fadeIn");
		});
		$("#tytydetailclose").click(function(){
			$("#tytydetail").hide(1000);
			$('#tytydetail').removeClass();
			$("#asthersticker").show(1000);
			$("#ofisticker").show(1000);
			$("#oktasticker").show(1000);
			$("#mizthreesticker").show(1000);
			$("#obenxsticker").show(1000);
		}); 
		$("#ofidetailbutton").click(function(){
			$("#asthersticker").hide(1000);
			$("#tytysticker").hide(1000);
			$("#oktasticker").hide(1000);
			$("#mizthreesticker").hide(1000);
			$("#obenxsticker").hide(1000);
			$('#ofidetail').removeClass();
			$("#ofidetail").css("display","block");
			$("#ofidetail").addClass("fadeIn");
		});
		$("#ofidetailclose").click(function(){
			$("#ofidetail").hide(1000);
			$('#ofidetail').removeClass();
			$("#asthersticker").show(1000);
			$("#tytysticker").show(1000);
			$("#oktasticker").show(1000);
			$("#mizthreesticker").show(1000);
			$("#obenxsticker").show(1000);
		}); 
		$("#oktadetailbutton").click(function(){
			$("#asthersticker").hide(1000);
			$("#ofisticker").hide(1000);
			$("#tytysticker").hide(1000);
			$("#mizthreesticker").hide(1000);
			$("#obenxsticker").hide(1000);
			$('#oktadetail').removeClass();
			$("#oktadetail").css("display","block");
			$("#oktadetail").addClass("fadeIn");
		});
		$("#oktadetailclose").click(function(){
			$("#oktadetail").hide(1000);
			$('#oktadetail').removeClass();
			$("#asthersticker").show(1000);
			$("#ofisticker").show(1000);
			$("#tytysticker").show(1000);
			$("#mizthreesticker").show(1000);
			$("#obenxsticker").show(1000);
		}); 
		$("#mizthreedetailbutton").click(function(){
			$("#asthersticker").hide(1000);
			$("#ofisticker").hide(1000);
			$("#oktasticker").hide(1000);
			$("#tytysticker").hide(1000);
			$("#obenxsticker").hide(1000);
			$('#mizthreedetail').removeClass();
			$("#mizthreedetail").css("display","block");
			$("#mizthreedetail").addClass("fadeIn");
		});
		$("#mizthreedetailclose").click(function(){
			$("#mizthreedetail").hide(1000);
			$('#mizthreedetail').removeClass();
			$("#asthersticker").show(1000);
			$("#ofisticker").show(1000);
			$("#oktasticker").show(1000);
			$("#tytysticker").show(1000);
			$("#obenxsticker").show(1000);
		}); 
		$("#obenxdetailbutton").click(function(){
			$("#asthersticker").hide(1000);
			$("#ofisticker").hide(1000);
			$("#oktasticker").hide(1000);
			$("#mizthreesticker").hide(1000);
			$("#tytysticker").hide(1000);
			$('#obenxdetail').removeClass();
			$("#obenxdetail").css("display","block");
			$("#obenxdetail").addClass("fadeIn");
		});
		$("#obenxdetailclose").click(function(){
			$("#obenxdetail").hide(1000);
			$('#obenxdetail').removeClass();
			$("#asthersticker").show(1000);
			$("#ofisticker").show(1000);
			$("#oktasticker").show(1000);
			$("#mizthreesticker").show(1000);
			$("#tytysticker").show(1000);
		}); 
	</script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>