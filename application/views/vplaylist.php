<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <meta name="Title" content="Apel Band Official Website">
	<meta name="Author" content="Apel Band">
	<meta name="Subject" content="Apel Band Official Website">
	<meta name="Description" content="Apel Band Official Website">
	<meta name="Keywords" content="Apel Band, Karma Cinta, Apelband, apel band, apel, band">
	<meta name="Language" content="English">
	<meta name="Copyright" content="Copyright 2015, Apel Band, Powered by Sidechain Labs">
	<meta name="Designer" content="Apel Band, Sidechain Labs">
	<meta name="Publisher" content="Apel Band, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/images/favicon.png" />
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	
	
  </head>
  <body style="background: url(<?php echo base_url(); ?>assets/images/<?php echo $background; ?>) no-repeat center center fixed;">
    <?php $this->load->view("vheader.php");?>
	<br><br><br>
	<div class="container">
		<div class="panel">
			<div class="row">
				<div class="col-lg-12">
					<div class="title">
						<h4>PLAYLIST</h4>
					</div>
				</div>
			</div>
			<div class="row">
				<?php 
					foreach($qplaylist as $row): 
					if($row->PLAYLISTTYPE=="1"){
				?>
				<div class="col-lg-4 text-center">
					<div class="title">
						<?php echo $row->PLAYLISTEMBED;?>
					</div>
				</div>
				<?php }endforeach; ?>
			</div>
			<div class="row">
				<?php 
					foreach($qplaylist as $row):
					if($row->PLAYLISTTYPE=="2"){
				?>
				<div class="col-lg-6 text-center">
					<div class="title">
						<?php echo $row->PLAYLISTEMBED;?>
					</div>
				</div>
				<?php }endforeach;?>
			</div>
			<br><br>
			<div class="row">
				<div class="col-lg-12">
					<div class="title">
						<h4>LYRICS</h4>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3">
					<div class="content-paragraf-lyric">
						<h4>Karma Cinta</h4>
						<h5>music+lirik by Yogi 'Gaijin'</h5>
						<p><br>		         
Kamu telah menyakiti hatiku<br>
Saatku terlanjur mencintaimu..<br>
Sepenuh hatiku..<br>
<br>
Dia.. kau tahu kumencintainya<br>
Dan kurela meninggalkan dia<br>
Hanya untuk kamu..<br>
<br>
CHORUS :<br>
Kau tahu ku sangat mencintainnya<br>
Tapi kurela meninggalkannya<br>
Demi dirimu, kulepaskan semua<br>
Cerita cintaku bersama dia..<br>
<br>
Kini.. kau seakan tiada peduli<br>
Di setiap apa yang kurasakan<br>
Inikah Karmaku..<br>
<br>
back to CHORUS<br>
<br>
Kau tahu ku sangat mencintainnya<br>
Tapi kurela meninggalkannya<br>
Demi dirimu, kulepaskan semua<br>
Cerita cintaku bersama dia..<br>
<br>
Oh.. Tetapi Karma yang kini terjadi<br>
Kuharus merelakanmu pergi<br>
Demi dirinya yang kini kau cinta<br>
Ini suratan yang harus kuterima..<br>
</p>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="content-paragraf-lyric">
						<h4>Kau Putuskan Ku (KPK)</h4>
						<h5>Cipt : Kenny Tarzan Boys</h5>
						<p><br>
Kemarin kau putuskan aku,dan aku terima itu keputusanmu..<br>
Aku bukan pacarmu lagi,terserah diriku,dekat sama siapa...<br>
<br>
Reff :<br>
<br>
tak usah dekati aku lagi,karna aku tak mungkin kembali padamu..<br>
tak usah rayu2 aku lagi,takkan kubuka lagi,kisah kita game over..game over..<br>
<br>
<br>
tak mungkin kita balikan lagi,maaf kutak bisa,aku tak cinta lagi..<br>
aku sudah ada yang punya,asal tahu saja,lebih dari dirimu..<br>
<br>
Reff 2x<br>

</p>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="content-paragraf-lyric">
						<h4>Resahku</h4>
						<h5>Cipt : Ofi Apel</h5>
						<p><br>
Ketika ku bertanya<br>
Salah apakah diriku<br>
Hingga buatmu terluka<br>
<br>
Ketika ku mencoba<br>
Tuk mencari jawabnya<br>
Kau malah bertanya<br>
<br>
Mengapa semua<br>
Tak dapat kumengerti<br>
Resahku padamu<br>
<br>
Chorus :<br>
Mungkin ku tak mengerti<br>
Cinta ini tlah mati<br>
Saat kau duakan diriku<br>
Mungkin ku tak sadari<br>
Cinta kita tlah mati<br>
Saat kau tinggalkan hatiku pun menangis<br>
<br>
Chorus :<br>
Mungkin ku tak mengerti<br>
Cinta ini tlah mati<br>
Saat kau duakan diriku<br>
Mungkin ku tak sadari<br>
Cinta kita tlah mati<br>
Saat kau tinggalkan hatiku pun menangis<br>
Hatiku pun menangis<br>
Hatiku pun menangis<br>

</p>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="content-paragraf-lyric">
						<h4>Sedang Apa Kamu</h4>
						<h5>Cipt : Capoenk Asbak</h5>
						<p><br>
Sedang apa kamu<br>
Lagi apa kamu<br>
Di sini kurindu slalu ingin tahu<br>
<br>
Dimana dirimu<br>
Ingatkah denganku<br>
Sedikit cemburu karna kucinta kamu<br>
<br>
Reff : <br>
Aku bisa marah<br>
Bila kau sengaja lupakan aku<br>
Tak luangkan waktumu<br>
Walau sebentar saja<br>
Penting untuk aku<br>
Asal kutahu<br>
Sedang apa dirimu<br>
<br>
Sedang apa kamu<br>
Lagi apa kamu<br>
Di sini kurindu slalu ingin tahu<br>
Slalu kumemikirkanmu<br>
<br>
Resah hati tak menentu<br>
Bila kamu tiada disisiku<br>
Juga tiada kabar darimu<br>

</p>
					</div>
				</div>
			</div>
			<br><br><br>
		</div>
	</div>
	<br><br>
	<?php $this->load->view('vfooter.php');?>

	
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	<script>
        $(document).ready(function() {
			$('#blog-landing').pinterest_grid({
                no_columns: 3,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
            });

        });
    </script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  </body>
</html>