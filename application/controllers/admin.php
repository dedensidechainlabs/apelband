<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->model('mcontent');
		$this->load->helper(array('form', 'url','file'));
    }
	
	function index()
	{
		$data['title'] = 'Apel Band';
		$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
		$data['background'] = $getBackground->SETTINGVALUE;

		$this->load->view('vadminlogin',$data);
	}	

	function login()
	{
		$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
		$data['background'] = $getBackground->SETTINGVALUE;

		$txtusername = $this->input->post("txtusername");
		$txtpassword = $this->input->post("txtpassword");
		$qcekusername = $this->db->query("SELECT * FROM TUSER WHERE USERNAME='$txtusername' AND USERPASSWORD='$txtpassword'");
		$valcekusername = $qcekusername->num_rows();
		if($valcekusername == 1){
			$row = $qcekusername->row();
			$userlevel = $row->USERLEVEL;
			$this->load->library('session');
			$this->session->set_userdata('username', $txtusername);
			$this->session->set_userdata('userlevel', $userlevel);
			redirect('admin/dashboard/','refresh');
		}else{
			$data['title'] = "Apel Band";
			$data['error'] = "<h5 style='color:#ff0000;'>Invalid Username or Password!<br>Please Try Again!</h5>";
			$this->load->view('vadminlogin',$data);
		}
	}

	function dashboard()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Apel Band";
			$data['username'] = $this->session->userdata('username');
			$this->load->view('vadmindashboard',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function background()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Apel Band";
			$data['username'] = $this->session->userdata('username');
			$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
			$data['background'] = $getBackground->SETTINGVALUE;
			$this->load->view('vadminbackground',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveBackground()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
			$data['background'] = $getBackground->SETTINGVALUE;
			$images = $this->input->post('userfile');
				
			$config['upload_path'] = './assets/images/';
			$config['allowed_types'] = 'jpg|png|gif|jpeg';
			$config['max_size']	= '7000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'Apel Band';
				$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
				$data['background'] = $getBackground->SETTINGVALUE;
				$this->load->view('vadminbackground', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$settingValue = array(
					'SETTINGVALUE' => $file_name
				);
				$settingName = 'background';
				$this->mcontent->saveBackground($settingName, $settingValue);
				$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
				$data['background'] = $getBackground->SETTINGVALUE;
				redirect('admin/background','refresh');
			}
		}else{
			redirect('admin','refresh');
		}
	}

	function gallery()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Apel Band";
			$data['username'] = $this->session->userdata('username');
			$data['qgallery'] = $this->mcontent->getGallery();
			$this->load->view('vadmingallery',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function addGallery()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$this->load->view('vadminaddgallery',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveGallery()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$title = $this->input->post('txttitle');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$date = date("Y-m-d");
		
				
			$config['upload_path'] = './assets/images/content/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'Apel Band';
				$this->load->view('vadminaddgallery', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datacontent = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTIMAGES' => $file_name,
					'CONTENTTYPE' => 'images',
					'CONTENTDATE' => $date
				);
				$this->mcontent->saveGallery($datacontent);
				$data['qgallery'] = $this->mcontent->getGallery();
				$this->load->view('vadmingallery',$data);
			}
		}else{
			redirect('admin','refresh');
		}
	}
	
	function deleteGallery()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$id = $this->input->post("contentID");
			$selectImage = $this->input->post("contentImages");
			$path = 'assets/images/content/'.$selectImage;
			unlink($path);
			$deldata = $this->mcontent->deleteGallery($id);
			redirect('admin/gallery/');
		}else{
			redirect('admin','refresh');
		}
	}
	
	function video()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Apel Band";
			$data['username'] = $this->session->userdata('username');
			$data['qvideo'] = $this->mcontent->getVideo();
			$this->load->view('vadminvideo',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	function addVideo()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$this->load->view('vadminaddvideo',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveVideo()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$title = $this->input->post('txttitle');
			$slug = url_title($title, '-', TRUE);
			$video = $this->input->post('txtvideo');
			$date = date("Y-m-d");
		
				
		
				$datavideo = array(
					'CONTENTTITLE' => $title,
					'CONTENTSLUG' => $slug,
					'CONTENTFULL' => $video,
					'CONTENTTYPE' => 'videos',
					'CONTENTDATE' => $date
				);
				$this->mcontent->saveVideo($datavideo);
				$data['qvideo'] = $this->mcontent->getVideo();
				$this->load->view('vadminvideo',$data);
			
		}else{
			redirect('admin','refresh');
		}
	}
	
	function deleteVideo()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$id = $this->input->post("contentID");
			$deldata = $this->mcontent->deleteVideo($id);
			redirect('admin/video/');
		}else{
			redirect('admin','refresh');
		}
	}
	
	function schedule()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Apel Band";
			$data['username'] = $this->session->userdata('username');
			$data['qschedule'] = $this->mcontent->getSchedule();
			$this->load->view('vadminschedule',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function addSchedule()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$this->load->view('vadminaddschedule',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function saveSchedule()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$title = $this->input->post('txttitle');
			$date = $this->input->post('txtdate');
			
				
		
				$dataschedule = array(
					'SCHEDULETITLE' => $title,
					'SCHEDULEDATE' => $date					
				);
				$this->mcontent->saveSchedule($dataschedule);
				$data['qschedule'] = $this->mcontent->getSchedule();
				$this->load->view('vadminschedule',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function deleteSchedule()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$id = $this->input->post("scheduleID");
			$deldata = $this->mcontent->deleteSchedule($id);
			redirect('admin/schedule/');
		}else{
			redirect('admin','refresh');
		}
	}
	
	function playlist()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Apel Band";
			$data['username'] = $this->session->userdata('username');
			$data['qplaylist'] = $this->mcontent->getPlaylist();
			$this->load->view('vadminplaylist',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function addPlaylist()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$this->load->view('vadminaddplaylist',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	function editPlaylist()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$playlistID = $this->input->post('playlistID');
			$data['qplaylist'] = $this->mcontent->getEditPlaylist($playlistID);
			$this->load->view('vadmineditplaylist',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	function savePlaylist()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$title = $this->input->post('txttitle');
			$featured = $this->input->post('txtfeatured');
			$type = $this->input->post('txttype');
			$embed = $this->input->post('txtembed');
			$date = $this->input->post('txtdate');
			
				
		
				$dataplaylist = array(
					'PLAYLISTTITLE' => $title,
					'PLAYLISTFEATURED' => $featured,
					'PLAYLISTTYPE' => $type,
					'PLAYLISTEMBED' => $embed,
					'PLAYLISTDATE' => $date					
				);
				$this->mcontent->savePlaylist($dataplaylist);
				$data['qplaylist'] = $this->mcontent->getPlaylist();
				$this->load->view('vadminplaylist',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function updatePlaylist()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$id = $this->input->post('txtid');
			$title = $this->input->post('txttitle');
			$featured = $this->input->post('txtfeatured');
			$type = $this->input->post('txttype');
			$embed = $this->input->post('txtembed');
			$date = $this->input->post('txtdate');
				$dataplaylist = array(
					'PLAYLISTTITLE' => $title,
					'PLAYLISTFEATURED' => $featured,
					'PLAYLISTTYPE' => $type,
					'PLAYLISTEMBED' => $embed,
					'PLAYLISTDATE' => $date					
				);
				$this->mcontent->updatePlaylist($id, $dataplaylist);
				$data['qplaylist'] = $this->mcontent->getPlaylist();
				$this->load->view('vadminplaylist',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function deletePlaylist()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$id = $this->input->post("playlistID");
			$deldata = $this->mcontent->deletePlaylist($id);
			redirect('admin/playlist/');
		}else{
			redirect('admin','refresh');
		}
	}
	
	function story()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Apel Band";
			$data['username'] = $this->session->userdata('username');
			$data['qstory'] = $this->mcontent->getStoryAll();
			$this->load->view('vadminstory',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	function addStory()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$this->load->view('vadminaddstory',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	function saveStory()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$title = $this->input->post('txttitle');
			$content = $this->input->post('txtcontent');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$date = date("Y-m-d");
		
				
			$config['upload_path'] = './assets/images/apelstory/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'Apel Band';
				$this->load->view('vadminaddstory', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datastory = array(
					'STORYTITLE' => $title,
					'STORYSLUG' => $slug,
					'STORYIMAGES' => $file_name,
					'STORYCONTENT' => $content,
					'STORYDATE' => $date
				);
				$this->mcontent->saveStory($datastory);
				$data['qstory'] = $this->mcontent->getStory();
				$this->load->view('vadminstory',$data);
			}
		}else{
			redirect('admin','refresh');
		}
	}
	
	function deleteStory()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$id = $this->input->post("storyID");
			$storyImages = $this->input->post("storyImages");
			$deldata = $this->mcontent->deleteStory($id);
			$path = "assets/images/apelstory/".$storyImages;
			@chmod(base_url().'assets/images/apelstory/'.$storyImages, 0777);
			@unlink(base_url().'assets/images/apelstory/'.$storyImages);
			delete_files($path);
			unlink($path);
			redirect('admin/story/');
			//echo $path;
		}else{
			redirect('admin','refresh');
		}
	}
	
	function editStory()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$storyID = $this->input->post('storyID');
			$data['qstory'] = $this->mcontent->getEditStory($storyID);
			$this->load->view('vadmineditstory',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function updateStory()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Apel Band';
			$id = $this->input->post('txtid');
			$title = $this->input->post('txttitle');
			$slug = url_title($title, '-', TRUE);
			$images = $this->input->post('userfile');
			$content = $this->input->post('txtcontent');
			$date = $this->input->post('txtdate');
			
			$q = $this->db->query("SELECT * FROM TSTORY WHERE STORYID='$id'")->row();
			$storyImages = $q->STORYIMAGES;
			$path = "assets/images/apelstory/".$storyImages;
			@chmod(base_url().'assets/images/apelstory/'.$storyImages, 0777);
			@unlink(base_url().'assets/images/apelstory/'.$storyImages);
			delete_files($path);
			unlink($path);
			
			$config['upload_path'] = './assets/images/apelstory/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				$error = array('error' => $this->upload->display_errors());
				//echo $this->upload->display_errors();
				$data['title'] = 'Apel Band';
				$this->load->view('vadmineditstory', $error);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datastory = array(
					'STORYTITLE' => $title,
					'STORYSLUG' => $slug,
					'STORYCONTENT' => $content,
					'STORYIMAGES' => $file_name,
					'STORYDATE' => $date					
				);
				$this->mcontent->updateStory($id, $datastory);
				$data['qstory'] = $this->mcontent->getStory();
				$this->load->view('vadminstory',$data);
			}
		}else{
			redirect('admin','refresh');
		}
	}
	function logout(){
		$this->session->sess_destroy();
		redirect('admin','refresh');
	}
}