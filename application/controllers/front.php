<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model("mcontent");
		$this->load->library('pagination');
		$this->load->helper("url");
    }
	
	public function index()
	{
		$data['title'] = 'Apel Band';
		$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
		$data['background'] = $getBackground->SETTINGVALUE;
		
		$data["content"] = $this->mcontent->getContent();
		$data["latestStory"] = $this->mcontent->getLatestStory();
		$data["qschedule"] = $this->mcontent->getScheduleHome();
		$data["qplaylist"] = $this->mcontent->getPlaylistHome();
		$this->load->view('vindex',$data);
	}

	public function theband()
	{
		$data['title'] = 'Apel Band';
		$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
		$data['background'] = $getBackground->SETTINGVALUE;
		
		$this->load->view('vtheband',$data);
	}
	
	public function playlist()
	{
		$data['title'] = 'Apel Band';
		$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
		$data['background'] = $getBackground->SETTINGVALUE;
		
		$data["qplaylist"] = $this->mcontent->getPlaylist();
		$this->load->view('vplaylist',$data);
	}
	
	public function gallery()
	{
		$data['title'] = 'Apel Band';
		$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
		$data['background'] = $getBackground->SETTINGVALUE;
		
		
		$getContentFUll = $this->mcontent->getContentFull();
		
		$config = array();
        $config["base_url"] = base_url() . "gallery";
        $config["total_rows"] = $this->db->count_all("TCONTENT");
        $config["per_page"] = 12;
        $config["uri_segment"] = 2;
        $config['cur_tag_open'] = '&nbsp;<a class="current" style="border-color: #fc4fd5; background: #fc4fd5; color: white;">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';

 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["content"] = $this->mcontent->getAllContentPage($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 
        $this->load->view('vgallery',$data);
	}
	
	public function apelstory($page='', $slug='')
	{
		$data['title'] = 'Apel Band';
		$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
		$data['background'] = $getBackground->SETTINGVALUE;
		
		if($page=="page" || $page==""){

			$config = array();
	        $config["base_url"] = base_url() . "apelstory/page";
	        $config["total_rows"] = $this->db->count_all("TSTORY");
	        $config["per_page"] = 9;
	        $config["uri_segment"] = 3;
	        $config['cur_tag_open'] = '&nbsp;<a class="current" style="border-color: #fc4fd5; background: #fc4fd5; color: white;">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';

	 
	        $this->pagination->initialize($config);
	 
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["qstory"] = $this->mcontent->getStory($config["per_page"], $page);
	        $data["links"] = $this->pagination->create_links();

			$this->load->view('vapelstory',$data);
		}elseif($page=="detail"){
			$data["qstory"] = $this->mcontent->getApelStory($slug);
			$data["randomStory"] = $this->mcontent->getRandomStory($slug);
			$this->load->view('vapelstorydetail',$data);
		}
	
	}

	public function store()
	{
		$data['title'] = 'Apel Band';
		$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
		$data['background'] = $getBackground->SETTINGVALUE;
		
		$this->load->view('vstore',$data);
	}
	
	public function about()
	{
		$data['title'] = 'Apel Band';
		$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
		$data['background'] = $getBackground->SETTINGVALUE;
		
		$this->load->view('vabout',$data);
	}
	
	public function contact()
	{
		$data['title'] = 'Apel Band';
		$getBackground = $this->db->get_where('TSETTING', array('SETTINGNAME' => 'background'))->row();
		$data['background'] = $getBackground->SETTINGVALUE;
		
		$this->load->view('vcontact',$data);
	}
}