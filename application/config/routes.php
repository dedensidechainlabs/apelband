<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	http://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There area two reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router what URI segments to use if those provided

| in the URL cannot be matched to a valid route.

|

*/



$route['default_controller'] = 'front';

$route['theband'] = 'front/theband';

$route['playlist'] = 'front/playlist';

$route['gallery'] = 'front/gallery';

$route['apelstory'] = 'front/apelstory';

$route['store'] = 'front/store';

$route['about'] = 'front/about';

$route['contact'] = 'front/contact';

$route['apelstory/(:any)'] = 'front/apelstory/$1';

$route['404_override'] = '';

$route['(:any)'] = "front/$1";

$route['admin'] = "admin";
$route['admin/login'] = "admin/login";
$route['admin/dashboard'] = "admin/dashboard";
$route['admin/gallery'] = "admin/gallery";
$route['admin/addGallery'] = "admin/addGallery";
$route['admin/saveGallery'] = "admin/saveGallery";
$route['admin/deleteGallery'] = "admin/deleteGallery";
$route['admin/video'] = "admin/video";
$route['admin/addVideo'] = "admin/addVideo";
$route['admin/saveVideo'] = "admin/saveVideo";
$route['admin/deleteVideo'] = "admin/deleteVideo";
$route['admin/schedule'] = "admin/schedule";
$route['admin/addSchedule'] = "admin/addSchedule";
$route['admin/saveSchedule'] = "admin/saveSchedule";
$route['admin/deleteSchedule'] = "admin/deleteSchedule";
$route['admin/playlist'] = "admin/playlist";
$route['admin/addPlaylist'] = "admin/addPlaylist";
$route['admin/savePlaylist'] = "admin/savePlaylist";
$route['admin/deletePlaylist'] = "admin/deletePlaylist";
$route['admin/story'] = "admin/story";
$route['admin/addStory'] = "admin/addStory";
$route['admin/saveStory'] = "admin/saveStory";
$route['admin/deleteStory'] = "admin/deleteStory";
$route['admin/editStory'] = "admin/editStory";
$route['admin/updateStory'] = "admin/updateStory";
$route['admin/logout'] = "admin/logout";
$route['admin/background'] = "admin/background";
$route['admin/saveBackground'] = "admin/saveBackground";
$route['admin/(:any)'] = "admin/$2";

/* End of file routes.php */

/* Location: ./application/config/routes.php */